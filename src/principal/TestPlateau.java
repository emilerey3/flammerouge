package principal;

import static org.junit.Assert.*;

import org.junit.Test;

public class TestPlateau {

	/*
	 * test du constructeur de la classe Plateau
	 */
	@Test
	public void testConstructeur() {
		//Instruction de test
		Plateau p = new Plateau(40);
		//Assertion
		assertEquals("Le plateau devrait avoir 40 cases", 40, p.getCases().size());
	}

}
