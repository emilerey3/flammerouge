package principal;

import java.io.IOException;
import java.util.*;

/*
 * classe Jeu qui represente une partie
 */
public class Jeu {
	
	public static final int nbPioche = 3;
	public static final String separateurP = "------";
	public static final String separateurG = "===================================";
	
	public static final boolean debug = false;
	
	private Plateau plateau;
	private int nbJoueurs;
	private List<Joueur> joueurs;
	private List<Carte> piocheFatigue;
	private int numTour;
	private boolean gagne;
	private Joueur gagnant;
	
	/*
	 * constructeur qui permet la mise en place du jeu
	 */
	public Jeu(ArrayList<String> nomsJ, int nbC) {
		this.plateau = new Plateau(nbC);
		this.nbJoueurs = nomsJ.size();
		this.joueurs = new ArrayList<Joueur>();
		
		int caseDepart = this.nbJoueurs - 1;
		
		Case cRouleur,cSprinteur;
		for(int i = 0; i < this.nbJoueurs; i++) {
			this.joueurs.add(new Joueur(nomsJ.get(i)));
			
			//place le rouleur du joueur i a sa position de depart
			cRouleur = this.plateau.getCases().get(caseDepart - i);
			cRouleur.setC1(this.joueurs.get(i).getRouleur());
			this.joueurs.get(i).getRouleur().setPosition(caseDepart - i);
			
			//place le sprinteur du joueur i a sa position de depart
			cSprinteur = this.plateau.getCases().get(caseDepart - i);
			cSprinteur.setC2(this.joueurs.get(i).getSprinteur());
			this.joueurs.get(i).getSprinteur().setPosition(caseDepart - i);
		}
		
		this.piocheFatigue = new ArrayList<Carte>();
		for(int i = 0; i < 60; i++) {
			this.piocheFatigue.add(new CarteFatigue(2));
		}
		this.numTour = 0;
		this.gagne = false;
	}
	
	public void start() {
		Joueur j;
		Rouleur r;
		Sprinteur s;
		int val;
		System.out.println(this.plateau.toString());
		while(!this.gagne) {
			this.numTour++;
			System.out.println(separateurG);
			System.out.println(" NOUVEAU TOUR : TOUR N°" + this.numTour);
			System.out.println(separateurG);
			//Etape 1 : chaque joueur choisi une carte Energie pour chacun de ses cyclistes
			System.out.println("Étape 1 : Choix des cartes énergie (tour n°" + this.numTour + ")");
			System.out.println(separateurG);
			for(int i = 0; i < this.nbJoueurs; i++) {
				j = this.joueurs.get(i);
				System.out.println(separateurP);
				System.out.println("| C'est au tour de " + j.getNom());
				System.out.println("| Votre Rouleur se trouve sur la case " + j.getRouleur().getPosition());
				System.out.println("| Votre Sprinteur se trouve sur la case " + j.getSprinteur().getPosition());
				System.out.println("|");
				System.out.println("| Rappel du plateau actuel :");
				System.out.println("| " + this.plateau.toString());
				System.out.println("|");
				System.out.println("| " + j.getNom() + " doit choisir une carte Rouleur");
				System.out.println(separateurP);
				r = (Rouleur) j.getRouleur();
				r.setCarteActuelle(r.piocher());
				System.out.println(separateurP);
				System.out.println("| " + j.getNom() + " doit choisir une carte Sprinteur");
				System.out.println(separateurP);
				s = (Sprinteur) j.getSprinteur();
				s.setCarteActuelle(s.piocher());
			}
			//Etape 2 : faire avancer les cyclistes
			System.out.println(separateurG);
			System.out.println("Étape 2 : Avancée des cyclistes (tour n°" + this.numTour + ")");
			System.out.println(separateurG);
			int i = 0;
			while(i < this.nbJoueurs && !this.gagne) {
				j = this.joueurs.get(i);
				//Faire avancer le rouleur
				r = (Rouleur) j.getRouleur();
				val = r.getCarteActuelle().getValeur();
				this.avancerCycliste(r, val);
				//Faire avancer le sprinteur
				s = (Sprinteur) j.getSprinteur();
				val = s.getCarteActuelle().getValeur();
				this.avancerCycliste(s, val);
				
				i++;
			}
			if(debug){
				System.out.println("Positions AVANT gestion : " + this.plateau.toString());
			}
			//Suite du tour (prochaines étapes)
			if(!this.gagne) {
				//Étape 3 : gestion des aspirations
				System.out.println(separateurG);
				System.out.println("Étape 3 : Gestion des aspirations (tour n°" + this.numTour + ")");
				System.out.println(separateurG);
				this.gererAspirations();
				if(debug) {
					System.out.println("Positions APRÈS gestion : " + this.plateau.toString());
				}
			}
		}
		int totalVictoires = 1;
		try {
			totalVictoires = new Sauvegarde().nouveauVainqueur(this.gagnant.getNom());
		} catch (IOException e) {
			e.printStackTrace();
		}
		System.out.println(separateurG);
		System.out.println("La partie est terminée, " + this.gagnant.getNom() + " a gagné ! (total de ses victoires : " + totalVictoires + ")");
		System.out.println(separateurG);
	}
	
	/*
	 * methode permettant de deplacer un cycliste vers une case designee
	 * @param cycl qui designe le cycliste a deplacer
	 * @param numCase qui designe la case sur laquelle on souhaite deplacer le cycliste
	 */
	public void deplacerCycliste(Cycliste cycl, int numCase) {
		Case c = this.plateau.getCases().get(numCase);
		if(debug) {
			System.out.println("DEPLACEMENT : Cycliste de " + cycl.getJoueur().getNom() + " case actuelle : " + cycl.getPosition() + " va en " + numCase);
		}
		if (c.placeLibre()) {
			if(cycl.getPosition() >= 0) {
				Case cas = this.plateau.getCases().get(cycl.getPosition());
				if(cycl.equals(cas.getC1())) {
					cas.setC1(null);
				} else {
					cas.setC2(null);
				}
				cycl.setPosition(numCase);
				if (c.c1Libre()) {
					c.setC1(cycl);
				}else {
					c.setC2(cycl);
				}
			}
		}
	}
	
	/*
	 * methode permettant de faire avancer un cycliste d'un nomnbre de cases donné et qui vérifie si le cycliste dépasse la ligne d'arrivée
	 * @param cycl Cycliste à faire avancer
	 * @param nbCase nombre de cases dont le Cycliste doit avancer
	 */
	public void avancerCycliste(Cycliste cycl, int nbCase) {
		//Si le cycliste dépasse la ligne d'arrivée
		if((cycl.getPosition() + nbCase) >= this.plateau.getCases().size() - 1) {
			this.gagne = true;
			this.gagnant = cycl.getJoueur();
		} else {
			while((!this.plateau.getCases().get(cycl.getPosition() + nbCase).placeLibre()) && (cycl.getPosition() + nbCase) > cycl.getPosition()) {
				nbCase--;
			}
			if((cycl.getPosition() + nbCase) > cycl.getPosition()) {
				this.deplacerCycliste(cycl, cycl.getPosition() + nbCase);
			}
		}
	}
	
	public List<Carte> getPiocheFatigue() {
		return piocheFatigue;
	}
	
	public void setPiocheFatigue(List<Carte> piocheFatigue) {
		this.piocheFatigue = piocheFatigue;
	}
	
	public Plateau getPlateau() {
		return this.plateau;
	}
	
	/*
	 * Cette méthode permet de gérer les asporations sur le plateau en fonction de la position de chaque Cycliste
	 * Elle déplace automatiquement les cyclistes concernés
	 */
	public void gererAspirations() {
		ArrayList<Cycliste> peloton = new ArrayList<Cycliste>();
		Case caseActuelle;
		boolean casePrecVide = false;
		//"Scan" de toutes les cases du plateau
		for(int i = 0; i < this.plateau.getCases().size(); i++) {
			caseActuelle = this.plateau.getCases().get(i);
			if(casePrecVide) {
				if(caseActuelle.estOccupee()) {
					//Faire avancer le peloton
					for(int j = (peloton.size() - 1); j >= 0; j--) {
						this.avancerCycliste(peloton.get(j), 1);
					}
					casePrecVide = false;
				} else if(!peloton.isEmpty()) {
					this.donnerFatiguePeloton(peloton);
				}
			}
			if(peloton.isEmpty()) {
				if(!caseActuelle.c1Libre()) {
					peloton.add(caseActuelle.getC1());
				}
				if(!caseActuelle.c2Libre()) {
					peloton.add(caseActuelle.getC2());
				}
			} else {
				if(caseActuelle.estOccupee()) {
					if(!caseActuelle.c1Libre()) {
						peloton.add(caseActuelle.getC1());
					}
					if(!caseActuelle.c2Libre()) {
						peloton.add(caseActuelle.getC2());
					}
				} else {
					casePrecVide = true;
				}
			}
		}
	}
	
	/*
	 * Cette méthode permet de donner des cartes fatigue aux Cyclistes en tête du peloton passé en paramètre
	 * @param peloton liste de tous les joueurs du peloton
	 */
	public void donnerFatiguePeloton(ArrayList<Cycliste> peloton) {
		Case casePremierPeloton;
		Cycliste premierPeloton;
		premierPeloton = peloton.get(peloton.size() - 1);
		
		//Donner une carte fatigue
		this.ajouterCarteFatigue(premierPeloton);
		
		casePremierPeloton = this.plateau.getCases().get(premierPeloton.getPosition());
		if(!casePremierPeloton.c1Libre()) {
			if(!casePremierPeloton.getC1().equals(premierPeloton)) {
				//Donner une carte fatigue
				this.ajouterCarteFatigue(casePremierPeloton.getC1());
			}
		}
		if(!casePremierPeloton.c2Libre()) {
			if(!casePremierPeloton.getC2().equals(premierPeloton)) {
				//Donner une carte fatigue
				this.ajouterCarteFatigue(casePremierPeloton.getC2());
			}
		}
		peloton.clear();
	}
	
	/*
	 * Cette méthode permet d'ajouter une carte fatigue dans la pioche du Cycliste passé en paramètre
	 * @param cycl le Cycliste auquel on souhaite donner une carte fatigue
	 */
	public boolean ajouterCarteFatigue(Cycliste cycl) {
		boolean res = true;
		if(this.piocheFatigue.size() > 0) {
			System.out.println("Fatigue : Le " + cycl.getType()  + " de " + cycl.getJoueur().getNom() + " prend une carte Fatigue");
			cycl.ajouterCarte(this.piocheFatigue.get(0));
			this.piocheFatigue.remove(0);
		} else {
			System.out.println("Fatigue : Il n'y a plus de carte fatigue dans la pioche, le " + cycl.getType() + " de " + cycl.getJoueur().getNom() + " n'a donc pas de malus");
			res = false;
		}
		return res;
	}
}