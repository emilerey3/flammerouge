package principal;
/*
 * Classe Case qui represente une case du plateau, une case pouvant comporter deux cyclistes
 */
public class Case {
	
	/*
	 * deux attributs Cyclistes qui permettent de pouvoir placer un cycliste sur une case
	 */
	private Cycliste c1,c2; 
	
	
	public Case() {
		this.c1 = null;
		this.c2 = null;
	}
	

	public Cycliste getC1() {
		return this.c1;
	}
	
	public Cycliste getC2() {
		return this.c2;
	}
	
	/*
	 * methode permettant de placer un cycliste sur une place de la case
	 * @param c1 le cycliste a placer
	 */
	public void setC1(Cycliste c1) {
		this.c1 = c1;
	}
	
	/*
	 * methode permettant de placer un cycliste sur une place de la case
	 * @param c2 le cycliste a placer
	 */
	public void setC2(Cycliste c2) {
		this.c2 = c2;
	}

	/*
	 * methode permettant d indiquer si la place 1 de la case est libre ou non
	 * @return true si la place 1 est libre
	 */
	public boolean c1Libre() {
		boolean res = true;
		if (c1 != null) {
			res = false;
		}
		return res;
	}
	
	/*
	 * methode permettant d indiquer si la place 2 de la case est libre ou non
	 * @return true si la place 2 est libre
	 */
	public boolean c2Libre() {
		boolean res = true;  
		if (c2 != null) {
			res = false;
		}
		return res;
	}
	
	
	/*
	 * methode permettant d'indiquer si il y a au moins une place de libre sur la case
	 * @return true si la case est vide
	 */
	public boolean placeLibre() {
		boolean res = true;
		if (!this.c1Libre() && !this.c2Libre()) {
			res = false;
		}
		return res;
	}
	
	/* 
	 * methode permettant d'indiquer si une case est occupee, c-a-d si au moins une des deux places est occupee
	 * @return true si au moins une une place est occupee
	 */
	public boolean estOccupee() {
		boolean res = false;
		if (!this.c1Libre() || !this.c2Libre()) {
			res = true;
		}
		return res;
	}
	
}
