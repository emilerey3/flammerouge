package principal;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;

import org.junit.Test;

public class TestCase {

	/*
	 * test du constructeur d'une case, aucun cyliste n'étant placé sur la case a sa construction, les deux places doivent etre null
	 */
	@Test
	public void testConstructeur() {
		//Jeux de données
		//Instruction de test
		Case c = new Case();
		//Assertion
		assertEquals("la place c1 devrait etre null", null, c.getC1());
		assertEquals("la place c2 devrait etre null", null, c.getC2());
	}
	
	
	/*
	 * test de la methode setC1 qui est la meme que la methode setC2
	 */
	@Test
	public void testSetC1() {
		//Jeux de données
		Joueur j = new Joueur("Joueur");
		Rouleur r = new Rouleur(j);
		Case c = new Case();
		//Instruction de test
		c.setC1(r);
		//Assertion
		assertEquals("la place devrait etre occupee par un cycliste", r, c.getC1());
	}
	
	
	/*
	 * test de la methode C1Libre qui est la meme que la methode c2Libre, cas ou la case est libre
	 */
	@Test
	public void testC1Libre1() {
		//Jeux de données
		//Instruction de test
		Case c = new Case();
		//Assertion
		assertTrue("la place C1 devrait etre libre", c.c1Libre());
	}
	
	/*
	 * test de la methode C1Libre qui est la meme que la methode c2Libre, cas ou la case n'est pas libre
	 */
	@Test
	public void testC1Libre2() {
		//Jeux de données
		Joueur j = new Joueur("Joueur");
		Rouleur r = new Rouleur(j);
		Case c = new Case();
		//Instruction de test
		c.setC1(r);
		//Assertion
		assertFalse("la place C1 ne devrait pas etre libre", c.c1Libre());
	}

	
	
	/*
	 * test de la méthode placeLibre dans le cas ou la place est libre 
	 */
	@Test
	public void testPlaceLibre1() {
		//Jeux de données
		Joueur j = new Joueur("Joueur");
		Rouleur r = new Rouleur(j);
		Case c = new Case();
		//Instruction de test
		c.setC1(r);
		//Assertion
		assertTrue("La place devrait etre libre", c.placeLibre());
	}
	
	/*
	 * test de la méthode placeLibre dans le cas ou la place n'est pas libre 
	 */
	@Test
	public void testPlaceLibre2() {
		//Jeux de données
		Joueur j = new Joueur("Joueur");
		Rouleur r = new Rouleur(j);
		Sprinteur s = new Sprinteur(j);
		Case c = new Case();
		//Instruction de test
		c.setC1(r);
		c.setC2(s);
		//Assertion
		assertFalse("La place ne devrait pas etre libre", c.placeLibre());
		
	}
	
	
	/*
	 * test de la méthode estOccupee dans le cas ou la case est bien occupee
	 */
	@Test
	public void testEstOccupee1() {
		//Jeux de données
		Joueur j = new Joueur("Joueur");
		Rouleur r = new Rouleur(j);
		Case c = new Case();
		//Instruction de test
		c.setC1(r);
		//Assertion
		assertTrue("La place devrait etre occupée", c.estOccupee());
	}
	
	/*
	 * test de la méthode estOccupee dans le cas ou la case n'est pas occupee
	 */
	@Test
	public void testEstOccupee2() {
		//Jeux de données
		//Instruction de test
		Case c = new Case();
		//Assertion
		assertFalse("La place ne devrait pas etre occupée", c.estOccupee());
	}
	
}
