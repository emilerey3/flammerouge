package principal;

import java.util.*;

/*
 * classe Sprinteur qui represente un cycliste de type sprinteur 
 */
public class Sprinteur extends Cycliste {
	
	private final List<Integer> valeursSprinteur = Arrays.asList(2,3,4,5,9);
	
	public Sprinteur(Joueur j) {
		super(j);
		this.type = "Sprinteur";
	}
	
	/*
	 * methode permettant de reconstituer la pioche du sprinteur
	 */
	public void nouvellePioche() {
		this.pioche.clear();
		for(int i = 0; i < this.valeursSprinteur.size(); i++) {
			for(int j = 0; j < 3; j++) {
				this.pioche.add(new CarteEnergie(this.valeursSprinteur.get(i)));
			}
		}
	}

}
