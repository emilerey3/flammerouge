package principal;

import static org.junit.Assert.assertEquals;

import org.junit.Test;

public class TestJoueur {

	@Test
	public void testConstructeur() {
		//Jeux de données
		//Instruction de test
		Joueur j = new Joueur("Joueur");
		//Assertion
		assertEquals("Le nom du joueur devrait etre Joueur", "Joueur", j.getNom());
	}
}
