package principal;

/* 
 * Classe CarteFatigue qui represente une carte fatigue
 */
public class CarteFatigue extends Carte {
	
	private int valeur;

	public CarteFatigue(int v) {
		this.valeur = v;
	}
	
	public int getValeur() {
		return this.valeur;
	}
	
	public String toString() {
		return "Carte fatigue de valeur " + this.getValeur();
	}

}
