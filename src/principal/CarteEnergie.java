package principal;

/* 
 * Classe CarteEnergie qui represente une carte energie
 */

public class CarteEnergie extends Carte {
	
	// valeur qui represente le nombre de cases qu'un joueur peut se deplacer pour une carte donnee
	private int valeur;        
	
	public CarteEnergie(int v) {
		this.valeur = v;
	}
	
	public int getValeur() {
		return this.valeur;
	}
	
	public String toString() {
		return "Carte énergie de valeur " + this.getValeur();
	}
	
}
