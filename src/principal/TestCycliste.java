package principal;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;

import java.util.ArrayList;
import org.junit.Test;

/*
 * classe de test pour les Cyclistes
 */
public class TestCycliste {

	/*
	 * test de la methode getJoueur
	 */
	@Test
	public void testGetJoueur() {
		//Jeux de données
		Joueur j = new Joueur("Testeur");
		//Instruction de test
		Rouleur cycl = new Rouleur(j);
		//Assertion
		assertEquals("Le joueur ne correspond pas", j, cycl.getJoueur());
	}
	
	/*
	 * test de la methode getType
	 */
	@Test
	public void testGetType() {
		//Jeux de données
		Joueur j = new Joueur("Testeur");
		Rouleur roul = new Rouleur(j);
		Sprinteur spri = new Sprinteur(j);
		//Assertion
		assertEquals("Le cycliste devrait être du type Rouleur", "Rouleur", roul.getType());
		assertEquals("Le cycliste devrait être du type Sprinteur", "Sprinteur", spri.getType());
	}
	
	/*
	 * test de la methode getType
	 */
	@Test
	public void testPosition() {
		//Jeux de données
		Rouleur cycl = new Rouleur(new Joueur("Testeur"));
		//Assertion
		assertEquals("La position initialle du Cycliste devrait être -1", -1, cycl.getPosition());
		//Instruction de test
		cycl.setPosition(10);
		//Assertion
		assertEquals("La position du Cycliste devrait être 10", 10, cycl.getPosition());
	}
	
	/*
	 * test de la methode assezDeCarte
	 */
	@Test
	public void testAssezDeCarte() {
		//Jeux de données
		Rouleur cycl = new Rouleur(new Joueur("Testeur"));
		//Instruction de test
		cycl.setPioche(new ArrayList<Carte>());
		//Assertion
		assertFalse("Le Cycliste ne devrait pas avoir assez de carte dans sa pioche", cycl.assezDeCarte());
		//Instruction de test
		cycl.nouvellePioche();
		//Assertion
		assertTrue("Le Cycliste devrait avoir assez de carte dans sa pioche", cycl.assezDeCarte());
	}
	
	/*
	 * test de la methode melangerDefausse
	 */
	@Test
	public void testMelangerDefausse() {
		//Jeux de données
		Rouleur cycl = new Rouleur(new Joueur("Testeur"));
		//Instruction de test
		cycl.getDefausse().add(new CarteEnergie(1));
		//Assertion
		assertTrue("La pioche du Cycliste devrait être vide", cycl.getPioche().isEmpty());
		//Instruction de test
		cycl.melangerDefausse();
		//Assertion
		assertTrue("La defausse du Cycliste devrait être vide", cycl.getDefausse().isEmpty());
		assertFalse("La pioche du Cycliste ne devrait pas être vide", cycl.getPioche().isEmpty());
	}

}
