package principal;

import java.util.ArrayList;

/*
 * classe Plateau qui represente le plateau de jeu
 * il est constitue de cases dont le nombre est defini dans la classe Jeu
 */
public class Plateau {
	
	private ArrayList<Case> cases;
	
	public Plateau(int nbCases){
		this.cases = new ArrayList<Case>();
		for(int i = 0; i < nbCases; i++) {
			this.cases.add(new Case());
		}
	}
	
	public ArrayList<Case> getCases() {
		return this.cases;
	}
	
	public String toString() {
		String res = "";
		boolean precOccupee = false;
		for(int i = 0; i < this.cases.size(); i++) {
			res += "[" + i + ".";
			precOccupee = false;
			if(!this.cases.get(i).c1Libre()) {
				res += this.cases.get(i).getC1().getJoueur().getNom();
				if(this.cases.get(i).getC1().getType().equals("Rouleur")) {
					res += "-R";
				} else {
					res += "-S";
				}
				precOccupee = true;
			}
			if(!this.cases.get(i).c2Libre()) {
				if(precOccupee) {
					res += ":";
				}
				res += this.cases.get(i).getC2().getJoueur().getNom();
				if(this.cases.get(i).getC2().getType().equals("Rouleur")) {
					res += "-R";
				} else {
					res += "-S";
				}
			}
			res += "]";
		}
		return res;
	}
	
}
