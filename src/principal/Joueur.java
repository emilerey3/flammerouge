package principal;

/*
 * classe Joueur qui represente un joueur
 * chaque joueur est designe par sa couleur et son nom
 * et il possede deux cyclistes : un rouleur et un sprinteur
 */
public class Joueur {
	
	private String nom;
	private Cycliste rouleur,sprinteur;
	
	public Joueur(String n) {
		this.nom = n;
		this.rouleur = new Rouleur(this);
		this.sprinteur = new Sprinteur(this);
		
		this.rouleur.nouvellePioche();
		this.sprinteur.nouvellePioche();
	}
	
	public Cycliste getRouleur() {
		return rouleur;
	}
	
	public Cycliste getSprinteur() {
		return sprinteur;
	}
	
	public String getNom() {
		return nom;
	}

}
