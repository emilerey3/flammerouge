package principal;

import java.util.*;

public class Main {
	
	public static Clavier cl;
	public static boolean automatique;

	public static void main(String[] args) {
		cl = new Clavier();
		System.out.println("Choisissez un nombre de cases pour le plateau de jeu");
		int nbC = cl.getInt();
		System.out.println("Choisissez un nombre de joueurs pour la partie");
		int nbJ = cl.getInt();
		
		ArrayList<String> listeNoms = new ArrayList<String>();
		
		for(int i = 0; i < nbJ; i++) {
			System.out.println("Veuillez entrer le nom du joueur " + (i + 1));
			listeNoms.add(cl.getString());
		}
		
		System.out.println("Voulez-vous utiliser le mode de jeu automatique ? ('oui' ou 'non')");
		String res = cl.getString();
		if(res.equalsIgnoreCase("oui")) {
			automatique = true;
		} else {
			automatique = false;
		}
		
		new Jeu(listeNoms,nbC).start();
		cl.fermer();
		
	}

}
