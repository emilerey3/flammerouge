package principal;

import static org.junit.Assert.*;

import org.junit.Test;


/*
 * classe de test pour les cartes, seuls les constructeurs sont testés
 */
public class TestCarte {
	
	@Test
	public void testCarteEnergie() {
		//Jeux de données
		//Instruction de test
		CarteEnergie c = new CarteEnergie(5);
		//Assertions
		assertEquals("La carte devrait avoir une valeur de 5", 5, c.getValeur());
	}
	
	@Test
	public void testCarteFatigue() {
		//Jeux de données
		//Instruction de test
		CarteFatigue c = new CarteFatigue(2);
		//Assertions
		assertEquals("La carte devrait avoir une valeur de 2", 2, c.getValeur());
	}
}
	
