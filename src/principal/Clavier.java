package principal;

import java.util.Scanner;

public class Clavier {
	
	private Scanner sc;
	
	/*
	 * Le Clavier premet de centraliser toutes les requêtes des joueurs
	 * sur un seul Scanner ouvert dans l'objet Clavier associé au Jeu
	 */
	public Clavier() {
		this.sc = new Scanner(System.in);
	}
	
	/*
	 * Méthode permettant de fermer le Scanner associé à l'objet Clavier
	 */
	public void fermer() {
		this.sc.close();
	}
	
	/*
	 * Méthode retournant le prochain entier que l'utilisateur entrera au clavier.
	 * @return le prochain entier entré par l'utilisateur
	 */
	public int getInt() {
		return this.sc.nextInt();
	}
	
	/*
	 * Méthode retournant le prochain entier que l'utilisateur entrera au clavier.
	 * Cette méthode permet de gérer le cas du mode de jeu Automatique
	 * @return le prochain entier entré par l'utilisateur
	 */
	public int getInt(boolean automatique) {
		int res;
		if(automatique) {
			res = 1 + (int)(Math.random() * ((Jeu.nbPioche - 1) + 1));
			if(Jeu.debug) {
				System.out.println("- JOUEUR AUTOMATIQUE : Choix : " + res +" -");
			}
		} else {
			res = this.sc.nextInt();
		}
		return res;
	}
	
	/*
	 * Méthode retournant la prochaine chaîne de caractères que l'utilisateur entrera au clavier.
	 * @return la prochaine chaîne de caractères entré par l'utilisateur
	 */
	public String getString() {
		return this.sc.next();
	}

}
