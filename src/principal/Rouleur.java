package principal;

import java.util.*;

/*
 * classe Rouleur qui represente un cycliste de type rouleur 
 */
public class Rouleur extends Cycliste {
	
	private final List<Integer> valeursRouleur = Arrays.asList(3,4,5,6,7);
	
	public Rouleur(Joueur j) {
		super(j);
		this.type = "Rouleur";
	}
	
	/*
	 * methode permettant de reconstituer la pioche du rouleur
	 */
	public void nouvellePioche() {
		this.pioche.clear();
		for(int i = 0; i < this.valeursRouleur.size(); i++) {
			for(int j = 0; j < 3; j++) {
				this.pioche.add(new CarteEnergie(this.valeursRouleur.get(i)));
			}
		}
	}

}
