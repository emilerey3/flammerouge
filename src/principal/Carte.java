package principal;

/* 
 * Classe abstraite représentant une carte 
 */
public abstract class Carte {
	
	public abstract int getValeur();
	public abstract String toString();
	
}
