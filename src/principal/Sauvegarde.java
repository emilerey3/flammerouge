package principal;

import java.io.*;
import java.util.ArrayList;

public class Sauvegarde {
	
	private final String nomFichier = "victoires.txt";
	
	/*
	 * Cette méthode permet d'augmenter de 1 le score du vainqueur passé en paramètre
	 * @param nomVainqueur nom du vainqueur dont on souhaite augmenter le score de 1
	 */
	public int nouveauVainqueur(String nomVainqueur) throws IOException {
		String ligne;
		ArrayList<String> lLigne = new ArrayList<String>();
		BufferedReader fichierSource = new BufferedReader(new FileReader(nomFichier));
		int nouveauScore = 1;
		boolean trouve = false;
		while ((ligne = fichierSource.readLine()) != null) {
			if(ligne.startsWith(nomVainqueur)) {
				String score = ligne.substring(nomVainqueur.length() + 1);
				nouveauScore = Integer.parseInt(score);
				nouveauScore++;
				lLigne.add(nomVainqueur + ":" + nouveauScore);
				trouve = true;
			} else {
				lLigne.add(ligne);
			}
		}
		if(!trouve) {
			lLigne.add(nomVainqueur + ":1");
		}
		fichierSource.close();
		BufferedWriter fichierDestination = new BufferedWriter(new FileWriter(nomFichier));
		for(int i = 0; i < lLigne.size(); i++) {
			fichierDestination.write(lLigne.get(i));
			fichierDestination.newLine();
		}
		fichierDestination.close();
		return nouveauScore;
	}
}