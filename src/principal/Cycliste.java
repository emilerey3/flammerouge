package principal;

import java.util.ArrayList;

/* 
 * classe abstraite Cycliste qui represente un cycliste, ne definit cependant pas s il s agit d un rouleur ou d un sprinteur 
 */
public abstract class Cycliste {
	
	private int position;
	protected ArrayList<Carte> pioche,defausse;  //chaque cycliste possede une pioche et une defausse
	private Carte carteActuelle;   //carte que le cycliste du joueur possede a son tour de jeu
	private Joueur joueur;  //attribut qui permet de designer le joueur proprietaire du cycliste
	protected String type;
	
	public Cycliste(Joueur j) {
		this.position = -1; 
		this.pioche = new ArrayList<Carte>();
		this.defausse = new ArrayList<Carte>();
		this.carteActuelle = null;
		this.joueur = j;
		this.type = "Cycliste";
	}
	
	public abstract void nouvellePioche();
	
	/*
	 * methode permettant de piocher 3 cartes alors proposees au joueur qui un choisi une et remet les autres dans la defausse
	 * @return la carte choisie par le joueur 
	 */
	public Carte piocher() {
		if(!this.assezDeCarte()) {
			System.out.println("Il n'y a pas assez de cartes dans la pioche,");
			System.out.println("la defausse est mélangée dans la pioche");
			this.melangerDefausse();
		}
		// tirage des 4 cartes qui sont alors proposees au joueur
		ArrayList<Carte> propositions = new ArrayList<Carte>();
		System.out.println("Voici les cartes que vous avez piochées :");
		Carte piocheC;
		for(int i = 0; i < Jeu.nbPioche; i++) {
			piocheC = this.piocherUneCarte();
			propositions.add(piocheC);
			System.out.println(" [" + (i + 1) + "] " + piocheC.toString());
		}
		int choix = 0;
		boolean choisi = false;
		while(!choisi) {
			System.out.println("Choisissez en une en entrant un numéro entre 1 et " + Jeu.nbPioche + " : ");
			choix = Main.cl.getInt(Main.automatique);
			if(choix >= 1 && choix <= Jeu.nbPioche) {
				choisi = true;
			} else {
				System.out.println("Erreur, vous devez entrer une valeur comprise entre 1 et " + Jeu.nbPioche);
			}
		}

		// remise des cartes non choisies dans la pioche, et attribution de la carte choisie au resultat de la methode
		Carte res = propositions.get(choix - 1);
		propositions.remove(choix - 1);
		while(!propositions.isEmpty()) {
			this.defausse.add(propositions.get(0));
			propositions.remove(0);
		}
		System.out.println("Vous avez choisi une carte de valeur " + res.getValeur() + ", les autres sont placées dans la defausse");
		return res;
	}
	
	
	/*
	 * methode permettant de piocher une seule carte de facon aleatoire dans la pioche
	 * @return la carte piochee
	 */
	private Carte piocherUneCarte() {
		int max,alea;
		max = this.pioche.size() - 1;
		alea = (int) (Math.random() * max);
		Carte res = this.pioche.get(alea);
		this.pioche.remove(alea);
		return res;
	}
	
	/*
	 * methode permettant d indiquer s'il reste assez de carte dans la pioche
	 * @return true s'il reste asser de carte
	 */
	public boolean assezDeCarte() {
		boolean res = true;
		if(this.pioche.size() < Jeu.nbPioche) {
			res = false;
		}
		return res;
	}
	
	/*
	 * methode permettant de melanger toute la defausse, et de la remettre dans la pioche, la defausse redevient alors vide
	 */
	public void melangerDefausse() {
		int alea,max;
		while(!this.defausse.isEmpty()) {
			max = this.defausse.size() - 1;
			alea = (int) (Math.random() * max);
			Carte r = this.defausse.get(alea);
			this.pioche.add(r);
			this.defausse.remove(r);
		}
	}
	
	public String toString() {
		String res = this.type + " de " + this.joueur.getNom() + ", position : " + this.position + " pioche : ";
		for(int i = 0; i < this.pioche.size(); i++) {
			res += "[" + this.pioche.get(i) + "]";
		}
		return res;
	}
	
	public int getPosition() {
		return this.position;
	}
	
	public void setPosition(int position) {
		this.position = position;
	}
	
	public Carte getCarteActuelle() {
		return this.carteActuelle;
	}
	
	/*
	 * methode permettant de definir la carte actuelle d'un Cycliste et de defausse l'ancienne (si elle existe)
	 * @param c la nouvelle carte du Cycliste
	 */
	public void setCarteActuelle(Carte c) {
		if(this.carteActuelle != null) {
			this.defausse.add(this.carteActuelle);
		}
		this.carteActuelle = c;
	}
	
	public ArrayList<Carte> getPioche() {
		return pioche;
	}
	
	public void setPioche(ArrayList<Carte> pioche) {
		this.pioche = pioche;
	}
	
	public ArrayList<Carte> getDefausse() {
		return defausse;
	}
	
	public void setDefausse(ArrayList<Carte> defausse) {
		this.defausse = defausse;
	}
	
	public Joueur getJoueur() {
		return joueur;
	}
	
	public void ajouterCarte(Carte c) {
		this.pioche.add(c);
	}
	
	public String getType() {
		return type;
	}
	
}
